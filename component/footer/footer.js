Component({
  properties: { /* 组件的对外属性，是属性名到属性设置的映射表，属性设置中可包含三个字段 type value observer */
    type: { /* tel / url / version */
      type: String,
      value: 'version'
    },
    tel:{ /* phone */
      type: String,
      value: ''
    },
    url:{ /* url */
      type: String,
      value: ''
    },
    version:{ /* version */
      type: String,
      value: ''
    },
  },

  attached: function () { //在组件实例进入页面节点树时执行
    this.selectType(this.properties.type);
    this.checkElement(this.properties.type);
  },

  data: { // 这里是一些组件内部数据
    content:''
  },
  methods: { // 这里是一个自定义方法
    /**
     * @method
     * @description 类型列表
     */
    checkElement(type){
      /* console.log(type)
      console.log(this.data[type])
      let obj={
        a:1
      };
      console.log(obj['a'])
      */
      if(this.data[type]===''){
        console.log(`%c ${type +' is empty!!!'}`,'background:yellow;color:#ff0036;')
      }

      
    },
    /**
     * @method
     * @description 类型列表
     */
    selectType(type){
      let list={//每个type只能由一个事件元素 tel、url，多个事件元素 用多个组件组装
        tel:{
          allLink:true,
          row:[
            {
              col:[
                {
                  type:'text',
                  text:'如需帮助，请致电超级棱镜官方客服'
                },{
                  type:'event',
                  class:'tel',
                  text:this.data.tel
                },{
                  type:'text',
                  text:'(同微信)'
                }
              ]
            }
          ]
        },
        url:{
          allLink:false,
          row:[
            {
              col:[
                {
                  type:'text',
                  text:'电脑端后台请登录'
                },{
                  type:'event',
                  class:'tel',
                  text:this.data.url
                }
              ]
            }
          ]
        },
        version:{
          allLink:false,
          row:[
            { 
              col:[
                {
                  type:'text',
                  text:'superprism.cn'
                }
              ]
            },
            { 
              col:[
                {
                  type:'text',
                  text:'「超级棱镜」提供技术支持'
                },{
                  type:'event',
                  class:'tel',
                  text:this.data.version
                }
              ]
            }
          ]
        }
      };
      this.setData({
        content:list[type]
      })
    },
    /**
     * 设置按钮尺寸 
     */
    click() {
      console.log('click')
      if(this.data.tel){
        wx.makePhoneCall({
          phoneNumber:this.data.tel
        })
      }
    }
  }

})