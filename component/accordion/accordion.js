Component({
  options: {
    multipleSlots: true
  },
  properties: {
    left: {
      type: String,
      value: "",
    },
    right: {
      type: String,
      value: "",
    },
    content: {
      type: String,
      value: "",
    },
    iconLeft: { //是否显示左边图标
      type: Boolean,
      value: false,
    }
  },
  ready: function (res) {
    show: false
  },
  methods: {
    clickTitle: function () {
      this.setData({
        show: !this.data.show
      })
    }
  }
})