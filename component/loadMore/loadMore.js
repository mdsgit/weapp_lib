Component({
  properties: { /* 组件的对外属性，是属性名到属性设置的映射表，属性设置中可包含三个字段 type value observer */
    type: { /* close / loading / noMore */
      type: String,
      value: 'noMore'
    }
  },

  attached: function () { //在组件实例进入页面节点树时执行
  },

  data: { // 这里是一些组件内部数据
  },
  methods: { // 这里是一个自定义方法
    /**
     * @method
     * @description 类型列表
     */
    checkElement(type){
      /* console.log(type)
      console.log(this.data[type])
      let obj={
        a:1
      };
      console.log(obj['a'])
      */
    }

      
    }

})