Component({
  options: {
    multipleSlots: true
  },
  properties: {
    nodes: {
      type: Array,
      value: [],
    }
  },
  relations: {
    '../htmpParser': {
      type: 'parent',
      linked: function (target) {},
      linkChanged: function (target) {},
      unlinked: function (target) {}
    }
  },
  data: {

  },
  ready: function (res) {
    this.data.query = wx.createSelectorQuery().in(this);
  },
  methods: {

    /**
     * @method previewImage
     * @description 点击预览图片
     */
    previewImage(e) {
      let index = e.currentTarget.dataset.index;
      this.triggerEvent('preview', {
        index: index
      }, {
        bubbles: true,
        composed: true
      });
    },

    /**
     * @method navigateTo
     * @description 跳转页面
     */
    navigateTo(e) {
      let url = e.currentTarget.dataset.url;

      //跳转到网页
      if (url.indexOf('http://') !== -1) {
        wx.navigateTo({
          url: '/pages/webView/webView?url=' + url
        })
      }

      //跳转到小程序页面
      if (url.indexOf('/pages/') !== -1) {
        wx.navigateTo({
          url: url
        })
      }
    },

    /**
     * @method imgLoad
     * @description 图片加载成功
     */
    imgLoad(e) {
      /*       let detail = e.detail,
              currentTarget = e.currentTarget,
              index = currentTarget.dataset.itemindex;

            let key = `nodes[${index}].attribs.height`;

            this.setData({
              [key]: detail.height
            }) */

      /*      this.getRect(`#${currentTarget.id}`)
             .then(res => {
               let real_width = res.width;

               let optimal_height = (real_width * detail.height) / detail.width;

               this.setData({
                 [key]: optimal_height
               })
             }) */
    },

    /**
     * @method imgError
     * @description 图片加载失败
     */
    imgError(e) {},

    /**
     * @method videoError
     * @description 视频加载失败
     */
    videoError(e) {},

    /**
     * @method getRect
     * @description 获取节点信息
     */
    getRect: function (node) {
      return new Promise((resolve, reject) => {
        this.data.query.select(node).boundingClientRect(res => {
          resolve(res);
        }).exec()
      })

    },


  }
})