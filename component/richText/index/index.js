const htmlparser = require('./htmlparser.js');

/**
 * @description 小程序富文本解析
 * @constructor {String} rawHtml html5字符串
 * 
 * @class Parser
 */
class Parser {
  constructor(rawHtml) {
    this.rawHtml = rawHtml
  }

  /**
   * @method getNodeArray
   * @description 获取解析后的数据
   * @param {any} callBack
   * @memberof Parser
   */
  getNodeArray(callBack) {
    let handler = new htmlparser.DefaultHandler(callBack);
    let parser = new htmlparser.Parser(handler);
    parser.parseComplete(this.rawHtml);
  }
}


module.exports = Parser;