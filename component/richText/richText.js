const Parser = require('./index/index')
Component({
  options: {
    multipleSlots: true
  },
  properties: {
    html: {
      type: String,
      value: '',
      observer: function (newVal, oldVal) {
        if (newVal) {
          this.parserHtml();
        }
      }
    }
  },
  relations: {
    './htmpParser-item/htmpParser-item': {
      type: 'child',
      linked: function (target) {},
      linkChanged: function (target) {},
      unlinked: function (target) {}
    }
  },
  data: {
    imgList: []
  },

  ready: function (res) {
    /*
    //解析测试
    let that = this;
       wx.request({
         url: 'https://developers.weixin.qq.com/miniprogram/dev/component/web-view.html',
         data: {},
         method: 'GET', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
         // header: {}, // 设置请求的 header
         success: function (res) {
           // success
           that.data.html = res.data;
           that.parserHtml();
         },
         fail: function () {
           // fail
         },
         complete: function () {
           // complete
         }
       }) */
  },

  methods: {
    /**
     * @method parserHtml
     * @description 解析html
     */
    parserHtml() {
      let parser = new Parser(this.data.html);
      parser.getNodeArray((error, res) => {
        this.data.imgList = res.imgList;
        this.setData({
          nodes: res.dom
        })
      });
    },

    /**
     * @method previewImage
     * @description 点击预览图片
     */
    previewImage(e) {
      let imgList = this.data.imgList;
      let index = e.detail.index;
      wx.previewImage({
        current: imgList[index], // 当前显示图片的http链接
        urls: imgList // 需要预览的图片http链接列表
      })
    }
  }
})