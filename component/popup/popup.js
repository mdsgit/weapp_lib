Component({
  options: {
    multipleSlots: true
  },
  properties: {
    placement: {
      type: String,
      value: 'bottom',
    },
    visible: {
      type: Boolean,
      value: false,
    },
    modal: {
      type: Boolean,
      value: true,
    }
  },
  data: {
    translate: 'translate(0px,0px)',
    popupBody: {
      width: 0,
      height: 0,
    },
    loaded: true,
    shwoModal: false
  },
  ready: function (res) {
    // 组件生命周期函数，在组件布局完成后执行，此时可以获取节点信息（使用 SelectorQuery ）
    this.getRect('.popup-body')
      .then(res => {
        this.data.popupBody = res;
        this.setTranslate(); //设置弹出层偏移量
      })
  },
  methods: {
    /**
     * @method preventScoll
     * @description 防止背景滚动
     */

    preventScoll() {},
    /**
     * @method stopOnTap
     * @description 防止popup-body触发点击事件
     */
    stopOnTap() {},

    /**
     * @method setTranslate
     * @description 设置弹出层偏移量
     */
    setTranslate() {
      let {
        popupBody,
        placement
      } = this.data;
      let translate = null;
      switch (placement) {
        case 'top':
          translate = `translate(0px,-${popupBody.height}px)`;
          break;
        case 'right':
          translate = `translate(${popupBody.width}px,0px)`;
          break;
        case 'bottom':
          translate = `translate(0px,${popupBody.height}px)`;
          break;
        case 'left':
          translate = `translate(-${popupBody.width}px,0px)`;
          break;
      }
      this.setData({
        translate: translate,
      })

      setTimeout(() => {
        this.setData({
          loaded: false
        })
      }, 500);
    },

    /**
     * @method getRect
     * @description 获取节点信息
     */
    getRect(node) {
      return new Promise((resolve, reject) => {
        let query = wx.createSelectorQuery().in(this);
        query.select(node).boundingClientRect(res => {
          resolve(res);
        }).exec()
      })
    },

    /**
     * 点击模态框
     */
    onTap: function (e) {
      this.triggerEvent('tap');
    }
  }
})