Component({
  options: {
    //一些组件选项，请参见文档其他部分的说明
    multipleSlots: true // 在组件定义时的选项中启用多slot支持
  },
  properties: {
    // 这里定义了innerText属性，属性值可以在组件使用时指定
    type: {
      type: String,
      value: 'text',
    },
    confirmType: {
      type: String,
      value: 'done',
    },
    confirmHold:{
      type: Boolean,
      value: false
    },
    password:{
      type: Boolean,
      value: false
    },
    adjustPosition:{
      type: Boolean,
      value: true
    },
    maxlength:{
      type: Number,
      value: 140,
    },
    cursorSpacing:{
      type: Number,
      value: 0,
    },
    cursor:{
      type: Number,
      value:0 
    },
    selectionStart:{
      type: Number,
      value: -1,
    },
    selectionEnd:{
      type: Number,
      value: -1,
    },
    focus:{
      type: Boolean,
      value: false
    },
    disabled:{
      type: Boolean,
      value: false
    },
    placeholderStyle:{
      type: String,
      value: '',
    },
    placeholderClass:{
      type: String,
      value: '',
    },
    title: {
      type: String,
      value: '',
    },
    placeholder: {
      type: String,
      value: '',
    },
    value:{
      type:String,
      value:''
    }
  },
  data: {
    // 这里是一些组件内部数据
    width:''
  },
  attached: function (res) {
    // 组件生命周期函数，在组件实例进入页面节点树时执行
    this.switchRadius(this.properties.radius); //设置按钮类型         
  },
  ready: function (res) {
    // 组件生命周期函数，在组件布局完成后执行，此时可以获取节点信息（使用 SelectorQuery ）
    if (this.properties.title) {
      this.getRect('.title')
    }
  },
  methods: {
    bindinput: function (e) {
      this.data.value=e.detail.value;
      var myEventDetail = e.detail// detail对象，提供给事件监听函数
      var myEventOption = {
      } // 触发事件的选项
      this.triggerEvent('input', myEventDetail, myEventOption)
    },
    bindfocus: function (e) {
      var myEventDetail = e.detail// detail对象，提供给事件监听函数
      var myEventOption = {
      } // 触发事件的选项
      this.triggerEvent('focus', myEventDetail, myEventOption)
    },
    bindblur: function (e) {
      var myEventDetail = e.detail// detail对象，提供给事件监听函数
      var myEventOption = {
      } // 触发事件的选项
      this.triggerEvent('blur', myEventDetail, myEventOption)
    },
    bindconfirm: function (e) {
      var myEventDetail = e.detail// detail对象，提供给事件监听函数
      var myEventOption = {
      } // 触发事件的选项
      this.triggerEvent('confirm', myEventDetail, myEventOption)
    },
    /**
     * @method
     * @description 设置按钮圆角
     */
    switchRadius(val) {
      if (val != '') {
        this.setData({
          tagRadius: `tag-radius-${val}`
        })
      }
    },
    // 这里是一个自定义方法
    _customMethod: function () { },

    _getAllLi: function () {
      // 使用getRelationNodes可以获得nodes数组，包含所有已关联的custom-li，且是有序的
      var nodes = this.getRelationNodes('path/to/custom-li')
    },

    /**
     * @method getRect
     * @description 获取节点信息
     */
    getRect: function (node) {
      let query = wx.createSelectorQuery().in(this);
      query.select(node).boundingClientRect(res => {
        let sysMsg = wx.getSystemInfoSync();
        this.setData({
          width: sysMsg.windowWidth - res.width + 'px'
        })
      }).exec()
    },

    onTap: function () {
      var myEventDetail = {} // detail对象，提供给事件监听函数
      var myEventOption = {} // 触发事件的选项
      this.triggerEvent('myevent', myEventDetail, myEventOption)
    }
  }
})