Component({
  options: {
    multipleSlots: true
  },
  properties: {
    // 这里定义了innerText属性，属性值可以在组件使用时指定
    type: {
      type: String,
      value: 'one',
    },
    value: {
      type: String,
      value: '',
    },
    icon: {
      type: Boolean,
      value: false,
    },
    radius: {
      type: Boolean,
      value: false,
    },
    color: {
      type: String,
      value: 'red',
    }
  },
  data: {},
  ready: function (res) {},
  methods: {
    /**
     * @method inputKeyWords
     * @description 输入关键词
     */
    inputKeyWords(e) {
      this.data.value = e.detail.value;
    },
    /**
     * @method searchClick
     * @description 点击搜索按钮
     */
    searchClick(e) {
      this.triggerEvent('search', {
        value: this.data.value
      }, {});
    }
  }
})