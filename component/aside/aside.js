
/**
 * @component
 * @createDate 2018-04-12 13:19:58
 * @desc 组件模板
*/

Component({
  options: {
    //一些组件选项，请参见文档其他部分的说明
    multipleSlots: true // 在组件定义时的选项中启用多slot支持
  },
  properties: {
    // 这里定义了innerText属性，属性值可以在组件使用时指定
    innerText: {
      type: String,
      value: 'default value',
    }
  },
  relations: {
    // 组件间关系定义，参见 组件间关系
    '../aside-box/aside-box': {
      type: 'parent', // 关联的目标节点应为子节点
      linked: function (target) {
        // 每次有custom-li被插入时执行，target是该节点实例对象，触发在该节点attached生命周期之后
        console.log('parent linked')
      },
      linkChanged: function (target) {
        // 每次有custom-li被移动后执行，target是该节点实例对象，触发在该节点moved生命周期之后
        console.log('parent changed')
      },
      unlinked: function (target) {
        // 每次有custom-li被移除时执行，target是该节点实例对象，触发在该节点detached生命周期之后
        console.log('parent unlinked')
      }
    }
  },
  externalClasses: [], // 组件接受的外部样式类，参见 外部样式类
  data: {
    // 这里是一些组件内部数据
    _openAside: false,//aside 打开状态
    translate: '',//移动动画
  },
  created: function (res) {
    // 组件生命周期函数，在组件实例进入页面节点树时执行，注意此时不能调用 setData
  },
  attached: function (res) {
    // 组件生命周期函数，在组件实例进入页面节点树时执行
  },
  ready: function (res) {
    // 组件生命周期函数，在组件布局完成后执行，此时可以获取节点信息（使用 SelectorQuery ）
    this._getAllLi();
  },
  moved: function (res) {
    // 组件生命周期函数，在组件实例被移动到节点树另一个位置时执行
  },
  detached: function (res) {
    // 组件生命周期函数，在组件实例被从页面节点树移除时执行
  },
  methods: {
    // 这里是一个自定义方法
    _customMethod: function () { },
    _getAllLi: function () {
      // 使用getRelationNodes可以获得nodes数组，包含所有已关联的custom-li，且是有序的
      var nodes = this.getRelationNodes('../aside-box/aside-box');
      if (nodes && nodes.length > 0) {
        this.data.node=nodes[0];
        wx.createSelectorQuery().in(this).select('#aside').boundingClientRect(res=>{
          /* console.log('createSelectorQuery')          
          console.log(res) */
          nodes[0].data.asideWidth=res.width;
        }).exec();
      }
    },
    preventD(){},
    _closeAside(){
      this.setData({
        _openAside: false,
        translate: ''
      })
      this.data.node.data.triggerState=false;
      this.data.node.setData({
        _openAside:false
      })
    },
    _clickAside(){
      console.log('_clickAside')
    },
    onTap: function () {
      var myEventDetail = {} // detail对象，提供给事件监听函数
      var myEventOption = {} // 触发事件的选项
      this.triggerEvent('myevent', myEventDetail, myEventOption)
    }
  }
})
