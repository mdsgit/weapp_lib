
/**
 * @component
 * @createDate 2018-04-12 13:19:58
 * @desc 组件模板
*/

Component({
  options: {
    //一些组件选项，请参见文档其他部分的说明
    multipleSlots: true // 在组件定义时的选项中启用多slot支持
  },
  properties: {
    // 盒子高度，不设置侧边栏滑动有时底部也会滑动
    height: {
      type: String,
      value: '',
    },
    fixedNode:{
      type: String,
      value: '',
    }
  },
  relations: {
    // 组件间关系定义，参见 组件间关系
    '../aside/aside': {
      type: 'child', // 关联的目标节点应为子节点
      linked: function (target) {
        // 每次有custom-li被插入时执行，target是该节点实例对象，触发在该节点attached生命周期之后
        console.log('child linked')
      },
      linkChanged: function (target) {
        // 每次有custom-li被移动后执行，target是该节点实例对象，触发在该节点moved生命周期之后
        console.log('child changed')
      },
      unlinked: function (target) {
        // 每次有custom-li被移除时执行，target是该节点实例对象，触发在该节点detached生命周期之后
        console.log('child unlinked')
      }
    }
  },
  externalClasses: [], // 组件接受的外部样式类，参见 外部样式类
  data: {
    // 这里是一些组件内部数据
    someData: {},
    eventState: false,//事件开启状态
    child: null,// child 节点
    asideWidth: 0,//侧边栏宽度
    triggerWidth: 120,//触发事件宽度
    triggerState: false,//触发状态
    startWidth: 0,//开始位置
    currentWidth: 0,//当前位置
    moveWidth: 0,//移动宽度标记 (currentWidth-startWidth)
    _openAside: false
  },
  created: function (res) {
    // 组件生命周期函数，在组件实例进入页面节点树时执行，注意此时不能调用 setData

  },
  attached: function (res) {
    // 组件生命周期函数，在组件实例进入页面节点树时执行
    
  },
  ready: function (res) {
    // 组件生命周期函数，在组件布局完成后执行，此时可以获取节点信息（使用 SelectorQuery ）
    this._getAllLi();
    console.log(this.data)
    if (this.data.height === '') {
      // console.log('height = null');
      let sysMsg = wx.getSystemInfoSync();
      if (this.data.fixedNode !== '') {
        console.log('fixedNode ='+this.data.fixedNode)
        wx.createSelectorQuery().select('#id').boundingClientRect(res => {
          // console.log(res)
          this.setData({
            height: sysMsg.windowHeight-res.height + 'px'
          })
        }).exec();
      } else {
        this.setData({
          height: sysMsg.windowHeight + 'px'
        })
      }

    }
  },
  moved: function (res) {
    // 组件生命周期函数，在组件实例被移动到节点树另一个位置时执行
  },
  detached: function (res) {
    // 组件生命周期函数，在组件实例被从页面节点树移除时执行
  },
  methods: {
    // 这里是一个自定义方法
    _customMethod: function () { },
    /**
     * @method
     * @description 开始滑动
     */
    _touchstart(e) {
      // console.log('_touchstart')
      this.data.startWidth = this.data.currentWidth = e.touches[0].pageX;//标记初始位置
    },
    /**
     * @method
     * @description 滑动中
     */
    _touchmove(e) {
      // console.log('_touchmove')
      let node = this.data.child;
      // let {currentWidth,startWidth,moveWidth} = this.data;
      this.data.currentWidth = e.touches[0].pageX;//标记当前位置
      //若移动宽度<=侧边栏宽度 或 当前位置<起始位置(即向左滑动)
      if (this.data.moveWidth <= this.data.asideWidth || (this.data.currentWidth - this.data.startWidth < 0)) {
        this.data.moveWidth = this.data.currentWidth - this.data.startWidth;//重置移动宽度
        // this.data.moveWidth=this.data.currentWidth-this.data.startWidth+this.data.moveWidth;
        node.setData({
          translate: 'transform: translateX(' + (this.data.moveWidth) + 'px)'//侧边栏滑动移动距离
        })
        // this.data.startWidth=this.data.currentWidth;
      };
      // 若移动距离>触发事件距离，则标记触发状态true
      if (this.data.moveWidth > this.data.triggerWidth) {
        this.data.triggerState = true;
      }
      // 当前位置<起始位置(即向左滑动)，则标记触发状态false
      if (this.data.startWidth > this.data.currentWidth) {
        this.data.triggerState = false;
      }
    },
    /**
     * @method
     * @description 滑动结束
     */
    _touchend(e) {
      console.log('_touchend')
      this.data.moveWidth = this.data.startWidth = this.data.currentWidth = 0;
      if (this.data.triggerState) {
        this._openAside()
      } else {
        this._closeAside()
      }
    },
    /**
     * @method
     * @description 打开侧边栏
     */
    _openAside(e) {
      console.log('openAside')
      let node = this.data.child;
      this.data.triggerState = true;
      this.setData({
        _openAside: true
      })
      node.setData({
        _openAside: true,
        translate: ''
      })
    },
    /**
     * @method
     * @description 关闭侧边栏
     */
    _closeAside(e) {
      console.log('closeAside')
      let node = this.data.child;
      this.data.triggerState = false;
      this.setData({
        _openAside: false
      })
      node.setData({
        _openAside: false,
        translate: ''
      })
    },
    _getAllLi: function () {
      // 使用getRelationNodes可以获得nodes数组，包含所有已关联的custom-li，且是有序的
      var nodes = this.getRelationNodes('../aside/aside');
      // console.log(this)      
      if (nodes && nodes.length > 0) {
        this.data.child = nodes[0];
        this.setData({
          eventState: true
        })
      }
      // console.log(this.data.eventState)
    },
    onTap: function () {
      var myEventDetail = {} // detail对象，提供给事件监听函数
      var myEventOption = {} // 触发事件的选项
      this.triggerEvent('myevent', myEventDetail, myEventOption)
    }
  }
})
