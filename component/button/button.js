Component({
  properties: { /* 组件的对外属性，是属性名到属性设置的映射表，属性设置中可包含三个字段 type value observer */
    size: { /* 尺寸  l / m / s / xs*/
      type: String,
      value: 'small'
    },
    type: { /* 类型 base /primary / success / warning / danger / info*/
      type: String,
      value: 'base'
    },
    plain: { /* 是否朴素按钮 */
      type: Boolean,
      value: false
    },
    radius: { /* 圆角 all/no/base */
      type: String,
      value: 'base'
    },
    disabled: { /* 是否禁用状态 */
      type: Boolean,
      value: false
    },
    color:{/* diy color */
      type: String,
      value: ''
    }
  },

  attached: function () { //在组件实例进入页面节点树时执行
    if(!this.properties.color){
      this.switchType(this.properties.type); //设置按钮类型 
    }
    this.switchSize(this.properties.size); //设置按钮类型 
    this.switchRadius(this.properties.radius); //设置按钮类型     
  },

  data: { // 这里是一些组件内部数据
    btnType: "", //按钮类型 
    btnSize: "", //按钮尺寸 
    btnRadius: "", //按钮尺寸
    hoverStyle:false,
    formId:''
  },
  methods: { // 这里是一个自定义方法
    onTap: function () {
      var myEventDetail = {
        formId:this.data.formId        
      } // detail对象，提供给事件监听函数
      var myEventOption = {
      } // 触发事件的选项
      this.triggerEvent('Click', myEventDetail, myEventOption)
    },
    /**
     * @method switchType
     * @description 设置按钮类型 
     */
    switchType(val) {
      if (val != '') {
        this.setData({
          btnType: `btn-${val}`
        })
      }
    },
    /**
     * @method
     * @description 设置按钮圆角
     */
    switchRadius(val) {
      if (val != '') {
        this.setData({
          btnRadius: `btn-radius-${val}`
        })
      }
    },
    /**
     * @method
     * @description 自定义 hover-style
     */
    catchStart(){
      // console.log('catchStart')
      this.setData({
        hoverStyle:true
      })
    },
    catchEnd(){
      // console.log('catchEnd')      
      this.setData({
        hoverStyle:false
      })
    },
    /**
     * @method
     * @description 获取formId
     */
    formSubmit(e){
      this.setData({
        formId:e.detail.formId
      })   
    },
    /**
     * 设置按钮尺寸 
     */
    switchSize(val) {
      if (val != '') {
        this.setData({
          btnSize: `btn-${val}`
        })
      }
    }
  }

})