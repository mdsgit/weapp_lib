Component({
  options: {
    multipleSlots: true
  },
  properties: {
    color: {
      type: String,
      value: '',
    },
    active: {
      type: Boolean,
      value: false,
    }
  },
  relations: {
    '../timeline': {
      type: 'parent',
    }
  },
  data: {
    active: false,
    headColor: '',
  },

  ready: function (res) {
    let rgb = this._hexToRgb(this.data.color, 0.5);
    this.setData({
      headColor: rgb
    })
  },
  methods: {
    /**
     * @method hexToRgb
     * @description 颜色16进制转rgba
     */
    _hexToRgb(hex, alpha = 1) {
      var sColor = hex.toLowerCase();
      var reg = /^#([0-9a-fA-f]{3}|[0-9a-fA-f]{6})$/;
      if (sColor && reg.test(sColor)) {
        if (sColor.length === 4) {
          var sColorNew = "#";
          for (var i = 1; i < 4; i += 1) {
            sColorNew += sColor.slice(i, i + 1).concat(sColor.slice(i, i + 1));
          }
          sColor = sColorNew;
        }
        var sColorChange = [];
        for (var i = 1; i < 7; i += 2) {
          sColorChange.push(parseInt("0x" + sColor.slice(i, i + 2)));
        }
        return `rgba(${sColorChange.join(",")},${alpha})`;
      }
      return sColor;
    }
  }
})