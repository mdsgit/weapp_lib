Component({
  options: {
    //一些组件选项，请参见文档其他部分的说明
    multipleSlots: true // 在组件定义时的选项中启用多slot支持
  },
  properties: {
    // 这里定义了innerText属性，属性值可以在组件使用时指定
    innerText: {
      type: String,
      value: 'default value',
    }
  },
  relations: {
    // 组件间关系定义，参见 组件间关系
    'timeline-item/timeline-item': {
      type: 'child', // 关联的目标节点应为子节点
      linked: function (target) {
        // 每次有custom-li被插入时执行，target是该节点实例对象，触发在该节点attached生命周期之后
      },
      linkChanged: function (target) {
        // 每次有custom-li被移动后执行，target是该节点实例对象，触发在该节点moved生命周期之后
      },
      unlinked: function (target) {
        // 每次有custom-li被移除时执行，target是该节点实例对象，触发在该节点detached生命周期之后
      }
    }
  },
  externalClasses: [], // 组件接受的外部样式类，参见 外部样式类
  data: {
    // 这里是一些组件内部数据
    someData: {}
  },

  ready: function (res) {},

  methods: {

    _getAllLi: function () {
      let nodes = this.getRelationNodes('timeline-item/timeline-item');
    },

    /**
     * @method hexToRgb
     * @description 颜色16进制转rgba
     */
    _hexToRgb(hex, alpha = 1) {
      var sColor = hex.toLowerCase();
      var reg = /^#([0-9a-fA-f]{3}|[0-9a-fA-f]{6})$/;
      if (sColor && reg.test(sColor)) {
        if (sColor.length === 4) {
          var sColorNew = "#";
          for (var i = 1; i < 4; i += 1) {
            sColorNew += sColor.slice(i, i + 1).concat(sColor.slice(i, i + 1));
          }
          sColor = sColorNew;
        }
        var sColorChange = [];
        for (var i = 1; i < 7; i += 2) {
          sColorChange.push(parseInt("0x" + sColor.slice(i, i + 2)));
        }
        return `rgba(${sColorChange.join(",")},${alpha})`;
      }
      return sColor;
    }
  }
})