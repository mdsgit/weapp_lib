Component({
  options: {
    //一些组件选项，请参见文档其他部分的说明
    multipleSlots: true // 在组件定义时的选项中启用多slot支持
  },
  properties: {
    // 这里定义了innerText属性，属性值可以在组件使用时指定
    type:{ /* cart / c1-type1 / c1-type2 /c2-type1 /c2-type2 / cn-type1 */
      type: String,
      value: 'cart',
    },
    mode:{
      type:String,
      value:'aspectFill'
    },
    item:{
      type:Object,
      value:null
    }
  },
  data: {
    // 这里是一些组件内部数据
    box:'',
    img:'',
    content:'',
    name:'',
    script:'',
    else:'',
  },
  created: function (res) {
    // 组件生命周期函数，在组件实例进入页面节点树时执行，注意此时不能调用 setData
  },
  attached: function (res) {
    // 组件生命周期函数，在组件实例进入页面节点树时执行
    this.switchBox(this.properties.type); //设置按钮类型      
    this.switchImg(this.properties.type); //设置按钮类型        
    this.switchContent(this.properties.type); //设置按钮类型        
    this.switchName(this.properties.type); //设置按钮类型    
    this.switchScript(this.properties.type); //设置按钮类型
    this.switchElse(this.properties.type); //设置按钮类型    
  },
  ready: function (res) {
    // 组件生命周期函数，在组件布局完成后执行，此时可以获取节点信息（使用 SelectorQuery ）
  },
  moved: function (res) {
    // 组件生命周期函数，在组件实例被移动到节点树另一个位置时执行
  },
  detached: function (res) {
    // 组件生命周期函数，在组件实例被从页面节点树移除时执行
  },
  methods: {
    /**
     * @method
     * @description 设置边框
     */
    switchBox(val) {
      if (val != '') {
        this.setData({
          box: `box-${val}`
        })
      }
    },
    /**
     * @method
     * @description 设置边框
     */
    switchImg(val) {
      if (val != '') {
        this.setData({
          img: `img-${val}`
        })
      }
    },
    /**
     * @method
     * @description 设置边框
     */
    switchContent(val) {
      if (val != '') {
        this.setData({
          content: `content-${val}`
        })
      }
    },
    /**
     * @method
     * @description 设置边框
     */
    switchName(val) {
      if (val != '') {
        this.setData({
          name: `name-${val}`
        })
      }
    },
    /**
     * @method
     * @description 设置边框
     */
    switchScript(val) {
      if (val != '') {
        this.setData({
          script: `script-${val}`
        })
      }
    },
    /**
     * @method
     * @description 设置边框
     */
    switchElse(val) {
      if (val != '') {
        this.setData({
          else: `else-${val}`
        })
      }
    },
    /**
     * @method getRect
     * @description 获取节点信息
     */
    getRect: function (node) {
      let query = wx.createSelectorQuery().in(this);
      query.select(node).boundingClientRect(res => {
        console.log(res);
      }).exec()
    },

    onTap: function () {
      var myEventDetail = {} // detail对象，提供给事件监听函数
      var myEventOption = {} // 触发事件的选项
      this.triggerEvent('myevent', myEventDetail, myEventOption)
    }
  }
})