Component({
  options: {
    multipleSlots: true // 在组件定义时的选项中启用多slot支持
  },
  properties: {
    type: {
      type: String,
      value: 'one'
    },
    list: {
      type: Array,
      value: []
    },
    iconKey: {
      type: String,
      value: 'icon'
    },
    textKey: {
      type: String,
      value: 'text'
    },
  },
  data: {},
  ready: function (res) {},
  methods: {
    clickGrid(e) {
      let index = e.currentTarget.dataset.index;
      this.triggerEvent('grid', {
        index
      });
    }
  }
})