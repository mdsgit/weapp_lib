Component({
  options: {
    multipleSlots: true // 在组件定义时的选项中启用多slot支持
  },
  properties: {
    dot: {
      type: Boolean,
      value: false,
    },
    border: {
      type: Boolean,
      value: false,
    },
    gradient: {
      type: Boolean,
      value: false,
    },
    color: {
      type: String,
      value: '#f44'
    },
    size: {
      type: String,
      value: 'small'
    },
    value: {
      type: String,
      value: 0
    },
    max: {
      type: Number,
      value: 99
    },

  },
  data: {
    linearGradient: 'background: linear-gradient(to right, #fea810, #feca31)'
  },

  ready: function (res) {
    let {
      value,
      max
    } = this.data;
    value = value - max > 0 ? `${max}+` : value;
    this.setData({
      value
    });
  },
  methods: {

  }
})