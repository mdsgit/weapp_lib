Component({
  options: {
    multipleSlots: true
  },
  properties: {
    list: {
      type: Array,
      value: []
    },
    textKey: {
      type: String,
      value: 'text'
    },
    autoplay: {
      type: Boolean,
      value: true
    },
    circular: {
      type: Boolean,
      value: true
    },
    current: {
      type: Number,
      value: 0
    },
    interval: {
      type: Number,
      value: 4000
    },
    duration: {
      type: Number,
      value: 2000
    },
  },
  data: {
    // 这里是一些组件内部数据
  },
  ready: function (res) {
    // 组件生命周期函数，在组件布局完成后执行，此时可以获取节点信息（使用 SelectorQuery ）
  },
  methods: {

    /**
     * @method clickIcon
     * @description 点击图标
     */
    clickIcon: function (e) {
      this.triggerEvent('icon', {}, {})
    },

    /**
     * @method clickSwiper
     * @description 点击滑块
     */
    clickSwiper: function (e) {
      this.triggerEvent('notice', {
        index: e.currentTarget.dataset.index
      }, {})
    }
  }
})