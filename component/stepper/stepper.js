Component({
  options: {
    multipleSlots: true // 在组件定义时的选项中启用多slot支持
  },
  properties: {
    size: {
      type: String,
      value: 'middle'
    },
    type: {
      type: String,
      value: 'one'
    },
    value: {
      type: Number,
      value: 0,
    },
    step: {
      type: Number,
      value: 1,
    },
    min: {
      type: Number,
      value: 0,
    },
    max: {
      type: Number,
      value: Infinity,
    },
    input: {
      type: Boolean,
      value: true,
    },
    color: {
      type: String,
      value: '#bcbcbc',
    }
  },
  data: {
    addhover: false,
    reducehover: false,
  },
  ready: function (res) {
    let max = this.data.max;
    this.data.max = max === null ? Infinity : max;
    let value = this.filterNum(this.data);
    this.setData({
      value: value,
      newValue: value
    })
  },
  methods: {

    /**
     * @method inputNum
     * @description 输入数字
     */
    inputNum(e) {
      let value = parseInt(e.detail.value);
      this.data.value = Number.isNaN(value) ? this.data.min : value;
      let newValue = this.filterNum(this.data);
      this.setData({
        newValue: newValue
      });
      this.triggerEvent('change', {
        value: newValue
      });
    },

    /**
     * @method inputBlur
     * @description 输入失焦事件
     */
    inputBlur(e) {
      this.setData({
        value: this.data.newValue
      });
    },

    /**
     * @method deleteEvent
     * @description 删除事件
     */
    deleteEvent(e) {
      this.triggerEvent('delete', {
        value: this.data.value,
        'delete': true
      });
    },

    /**
     * @method filterNum
     * @description 判断数字大小
     */
    filterNum({
      value,
      max,
      min
    }) {
      if (value >= max) {
        return max
      }
      if (value <= min) {
        return min
      }
      return value;
    },

    /**
     * @method changeNum
     * @description 改变数字
     */
    changeNum: function (e) {
      let method = e.currentTarget.dataset.method;
      let value = this[method](this.data);
      this.setData({
        value,
        newValue: value
      });
      this.triggerEvent('change', {
        value
      });
    },

    /**
     * @method add
     * @description 加数量
     */
    add({
      value,
      max,
      step
    }) {
      value = value + step;
      return value >= max ? max : value;
    },

    /**
     * @method reduce
     * @description 减数量
     */
    reduce({
      value,
      min,
      step
    }) {
      value = value - step;
      return value <= min ? min : value;
    },
    /**
     * @method catchStart
     * @description
     */
    catchStart(e) {
      let method = e.currentTarget.dataset.method;
      this.setData({
        [`${method}hover`]: true
      })
    },
    /**
     * @method catchEnd
     * @description
     */
    catchEnd(e) {
      let method = e.currentTarget.dataset.method;
      this.setData({
        [`${method}hover`]: false
      })
    },

  }
})