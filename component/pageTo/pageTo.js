
/**
 * @component
 * @createDate 2018-04-11 15:17:51
 * @desc 组件模板
*/

Component({
    options: {
      //一些组件选项，请参见文档其他部分的说明
      multipleSlots: true // 在组件定义时的选项中启用多slot支持
    },
    properties: {
      url:{/* 跳转路径 must */
        type:String,
        value:''
      },
      /* param:{/* 参数 形如：'key:value,key:value' *\/
        type:String,
        value:''
      }, */
      type:{/* 跳转方式 navigate/redirect/switchTab/reLaunch/navigateBack */
        type:String,
        value:'navigate'
      },
      delte:{/* 当 open-type 为 'navigateBack' 时有效，表示回退的层数 */
        type:Number,
        value:1
      },
      class:{/* 指定点击时的样式类，当hover-class="none"时，没有点击态效果 */
        type:String,
        value:'none'
      },
      delte:{/*  */
        type:String,
        value:''
      }
    },
    relations: {
      // 组件间关系定义，参见 组件间关系
      './custom-li': {
        type: 'child', // 关联的目标节点应为子节点
        linked: function (target) {
          // 每次有custom-li被插入时执行，target是该节点实例对象，触发在该节点attached生命周期之后
        },
        linkChanged: function (target) {
          // 每次有custom-li被移动后执行，target是该节点实例对象，触发在该节点moved生命周期之后
        },
        unlinked: function (target) {
          // 每次有custom-li被移除时执行，target是该节点实例对象，触发在该节点detached生命周期之后
        }
      }
    },
    externalClasses: [], // 组件接受的外部样式类，参见 外部样式类
    data: {
      // 这里是一些组件内部数据
      someData: {}
    },
    created: function (res) {
      // 组件生命周期函数，在组件实例进入页面节点树时执行，注意此时不能调用 setData
    },
    attached: function (res) {
      // 组件生命周期函数，在组件实例进入页面节点树时执行
      // this._cancatParam();//将参数拼接至url后
    },
    ready: function (res) {
      // 组件生命周期函数，在组件布局完成后执行，此时可以获取节点信息（使用 SelectorQuery ）
    },
    moved: function (res) {
      // 组件生命周期函数，在组件实例被移动到节点树另一个位置时执行
    },
    detached: function (res) {
      // 组件生命周期函数，在组件实例被从页面节点树移除时执行
    },
    methods: {
      // 这里是一个自定义方法
      _customMethod: function () {},
  
      _getAllLi: function () {
        // 使用getRelationNodes可以获得nodes数组，包含所有已关联的custom-li，且是有序的
        var nodes = this.getRelationNodes('path/to/custom-li')
      },
      /**
       * @method
       * @description 将参数拼接至url后
       */
      /* _cancatParam(){
        let str=this.properties.param
        // json=JSON.parse(str);
        str=str.replace(':','=')
        this.properties.url+='?'+str;
        console.log(this.properties.url)
      }, */
      onTap: function () {
        var myEventDetail = {} // detail对象，提供给事件监听函数
        var myEventOption = {} // 触发事件的选项
        this.triggerEvent('myevent', myEventDetail, myEventOption)
      }
    }
  })
