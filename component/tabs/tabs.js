Component({
  options: {
    multipleSlots: true
  },
  properties: {
    list: {
      type: Array,
      value: []
    },
    tabIndex: {
      type: Number,
      value: 0
    },
    lineWidth: {
      type: String,
      value: '',
    },
    selectedColor: {
      type: String,
      value: ''
    },
    textKey: {
      type: String,
      value: 'text'
    },
    type: {
      type: String,
      value: 'one'
    },
    tabsolt: {
      type: Boolean,
      value: true
    }
  },
  data: {
    loaded: false,
    translateX: 0,
    lineBoxWidth: 0,
    tabWidth: 25
  },
  ready: function (res) {
    let {
      tabIndex,
      list
    } = this.data;
    tabIndex = tabIndex - list.length >= 0 ? list.length - 1 : tabIndex;

    switch (this.data.type) {
      case 'one':
        this.setTabsLine(tabIndex);
        break;
      case 'two':
        this.setTabIndex(tabIndex)
        break;
    }
  },
  methods: {
    /**
     * @method clickTabOne
     * @description 点击标签
     */
    clickTabOne(e) {
      let index = e.currentTarget.dataset.index;
      this.setTabsLine(index);
    },

    /**
     * @method clickTabTwo
     * @description 点击标签
     */
    clickTabTwo(e) {
      let index = e.currentTarget.dataset.index;
      this.setTabIndex(index);
    },

    /**
     * @method setTabsLine
     * @description 设置下划线位置
     */
    setTabsLine(tabIndex) {
      let translateX = 0,
        length = this.data.list.length,
        tabWidth = length > 4 ? 25 : 100 / length;
      this.setData({
        tabWidth: tabWidth
      });
      Promise.all([this.getScrollView('.scroll-view-one'), this.getTabsList(`#nav${tabIndex}`)])
        .then(res => {
          let scroll = res[0],
            tab = res[1];
          translateX = scroll.scrollLeft + tab.left;
          let lineBoxWidth = tab.width;
          if (this.data.lineWidth) {
            this.setData({
              translateX: translateX,
              lineBoxWidth: lineBoxWidth,
            })
          } else {
            this.setData({
              translateX: translateX,
              lineBoxWidth: lineBoxWidth,
              lineWidth: `${lineBoxWidth}px`,
            })
          };
          this.setTabIndex(tabIndex);
        });
    },

    /**
     * @method setTabIndex
     * @description 设置下标
     */
    setTabIndex(tabIndex) {
      if (this.data.loaded) {
        this.setData({
          tabIndex: tabIndex
        });
      } else {
        this.data.loaded = true;
        setTimeout(() => {
          this.setData({
            tabIndex: tabIndex
          });
        }, 300);
      };
      this.triggerEvent('tab', {
        index: tabIndex
      });
    },

    /**
     * @method getScrollView
     * @description 获取 scroll-view 滚动距离
     */
    getScrollView(node) {
      return new Promise((resolve, reject) => {
        let query = wx.createSelectorQuery().in(this);
        query.select(node).fields({
          dataset: true,
          size: true,
          scrollOffset: true,
        }, res => {
          resolve(res)
        }).exec()
      })
    },

    /**
     * @method getTabsList
     * @description 获取 tab 位置
     */
    getTabsList(node) {
      return new Promise((resolve, reject) => {
        let query = wx.createSelectorQuery().in(this);
        query.select(node).boundingClientRect(res => {
          resolve(res)
        }).exec()
      })
    }
  }
})