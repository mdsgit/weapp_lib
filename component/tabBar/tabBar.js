Component({
  properties: {
    position: {
      type: String,
      value: 'bottom',
    },
    report: {
      type: Boolean,
      value: false,
    },
    tabBar: {
      type: Object,
      value: null,
      observer: function (newVal, oldVal) {
        if (oldVal && newVal.hide !== oldVal.hide) {
          let relationNodes = this.data.relationNodes,
            hide = newVal.hide,
            nodes = relationNodes ? relationNodes : this.getRelationNodes('../tabBar-item/tabBar-item'),
            len = nodes.length;
          for (let index = 0; index < len; index++) {
            const item = nodes[index];
            item.setData({
              hide: hide
            })
          }
        }
      }
    },
    navIndex: {
      type: Number,
      value: 0,
      observer: function (newVal, oldVal) {
        if (!this.data.navIndexChanged) {
          this.data.navIndexChanged = true;
        };
        this._setAllLiIndex(newVal);
      }
    },
    duration: {
      type: Number,
      value: 100,
    },
  },
  relations: {
    '../tabBar-item/tabBar-item': {
      type: 'child',
    }
  },
  data: {
    navIndexChanged: false,
    tabBarLoaded: false,
    relationNodes: null
  },
  ready: function (res) {
    this.setListWidth();
    if (!this.data.navIndexChanged) {
      this._setAllLiIndex(this.data.navIndex);
    }
  },
  methods: {

    /**
     * @method _setAllLiIndex
     * @description 设置子节点
     */
    _setAllLiIndex: function (navIndex) {
      let data = this.data,
        relationNodes = data.relationNodes,
        nodes = relationNodes ? relationNodes : this.getRelationNodes('../tabBar-item/tabBar-item'),
        len = nodes.length;
      if (!nodes[navIndex].data.loaded) {
        nodes[navIndex].setData({
          loaded: true
        })
      }
      if (!data.tabBarLoaded) {
        for (let index = 0; index < len; index++) {
          const item = nodes[index];
          item.setData({
            index: index,
            position: data.position,
            hide: data.tabBar.hide
          })
        }
        data.tabBarLoaded = true;
      }
    },

    /**
     * @method setListWidth
     * @description 当 list 长度超过 5 时，标签的长度设为 20%
     */
    setListWidth() {
      let list = this.data.tabBar.list,
        listLength = list.length;
      list.forEach(item => {
        item.num = item.num - 99 > 0 ? '99+' : item.num
      })
      let listWidth = listLength <= 5 ? 100 : 20;
      this.setData({
        listWidth,
        'tabBar.list': list,
        windowWidth: wx.getSystemInfoSync().windowWidth
      });
    },

    /**
     * @method getFormId
     * @description 获取formId
     */
    getFormId: function (e) {
      this.triggerEvent('getFormId', {
        formId: e.detail.formId
      });
    },

    /**
     * @method clickNav
     * @description 点击导航标签
     */
    clickNav(e) {
      let navIndex = e.currentTarget.dataset.index;
      this.setData({
        navIndex
      });
      this.triggerEvent('nav', {
        list: this.data.tabBar.list[navIndex],
        index: navIndex
      });
    }
  }
})