Component({
  options: {
    //一些组件选项，请参见文档其他部分的说明
    multipleSlots: true // 在组件定义时的选项中启用多slot支持
  },
  properties: {
    // 这里定义了innerText属性，属性值可以在组件使用时指定
    type:{ /* radio / checkbox */
      type: String,
      value: 'radio',
    },
    radius: { /* no / base / all */
      type: String,
      value: 'base',
    },
    color:{
      type: String,
      value: '',
    },
    list:{
      type:Array,
      value:[]
    }
  },
  data: {
    // 这里是一些组件内部数据
    checkRadius:''
  },
  created: function (res) {
    // 组件生命周期函数，在组件实例进入页面节点树时执行，注意此时不能调用 setData
  },
  attached: function (res) {
    // 组件生命周期函数，在组件实例进入页面节点树时执行
    this.switchRadius(this.properties.radius); //设置按钮类型         
    this.initList();
  },
  ready: function (res) {
    // 组件生命周期函数，在组件布局完成后执行，此时可以获取节点信息（使用 SelectorQuery ）
  },
  moved: function (res) {
    // 组件生命周期函数，在组件实例被移动到节点树另一个位置时执行
  },
  detached: function (res) {
    // 组件生命周期函数，在组件实例被从页面节点树移除时执行
  },
  methods: {
    /**
     * @method
     * @description 设置按钮圆角
     */
    switchRadius(val) {
      if (val != '') {
        this.setData({
          checkRadius: `check-radius-${val}`
        })
        console.log(this)
      }
    },
    /**
     * @method
     * @description 修改选择状态
     */
    selectState(e) {
      let index=e.currentTarget.dataset.index,
      list=this.data.list,
      ele=this.data.list[index],
      state=ele.select;
      if(this.data.type=='radio'){
        this.initList();
      }
      this.setData({
        [`list[${index}].select`]:!state
      })
    },
    /**
     * @method
     * @description 初始化list
     */
    initList(){
      let list=this.data.list;
      list.forEach(v=>{
        v.select=false;
      })
      this.setData({
        list:list
      })
    },
    // 这里是一个自定义方法
    _customMethod: function () {},

    _getAllLi: function () {
      // 使用getRelationNodes可以获得nodes数组，包含所有已关联的custom-li，且是有序的
    },

    /**
     * @method getRect
     * @description 获取节点信息
     */
    getRect: function (node) {
      let query = wx.createSelectorQuery().in(this);
      query.select(node).boundingClientRect(res => {
        console.log(res);
      }).exec()
    },

    onTap: function () {
      var myEventDetail = {} // detail对象，提供给事件监听函数
      var myEventOption = {} // 触发事件的选项
      this.triggerEvent('myevent', myEventDetail, myEventOption)
    }
  }
})