Component({
  options: {
    //一些组件选项，请参见文档其他部分的说明
    multipleSlots: true // 在组件定义时的选项中启用多slot支持
  },
  properties: {
    // 这里定义了innerText属性，属性值可以在组件使用时指定
    left: { //左边文字
      type: String,
      value: "",
    },
    right: { //右边文字
      type: String,
      value: "",
    },
    border: { //是否有边框
      type: Boolean,
      value: false,
    },
    iconLeft: { //是否显示左边图标
      type: Boolean,
      value: false,
    },
    iconRight: { //是否显示右边箭头
      type: Boolean,
      value: false,
    },
    list: {
      type: Array,
      value: [],
    },
    iconKey: {
      type: String,
      value: 'icon'
    },
    textKey: {
      type: String,
      value: 'text'
    },
  },
  data: {},
  ready: function (res) {
    this.setListWidth();
  },
  methods: {
    /**
     * @method setListWidth
     * @description 当 list 长度超过 5 时，标签的长度设为 20%
     */
    setListWidth() {
      let listLength = this.data.list.length;
      let listWidth = listLength <= 5 ? 100 / listLength : 20;
      this.setData({
        listWidth
      });
    },

    /**
     * @method clickList
     * @description 点击导航
     */
    clickList() {
      this.triggerEvent('list');
    },

    /**
     * @method clickNav
     * @description 点击子导航
     */
    clickNav(e) {
      this.triggerEvent('nav', {
        index: e.currentTarget.dataset.index
      });
    }

  }
})