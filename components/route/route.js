Component({
    properties: {
        // 在哪个目标上发生跳转，默认当前小程序，可选值self/miniProgram
        target: {
            type: String,
            value: 'self'
        },
        // 当前小程序内的跳转链接
        url: {
            type: String,
            value: ''
        },
        // 跳转方式 navigate/redirect/switchTab/reLaunch/navigateBack
        type: {
            type: String,
            value: 'reLaunch'
        },
        // 当 open-type 为 'navigateBack' 时有效，表示回退的层数
        delte: {
            type: Number,
            value: 1
        },
        // 指定点击时的样式类，当hover-class="none"时，没有点击态效果 
        class: {
            type: String,
            value: 'navigator-hover'
        },
    }
})