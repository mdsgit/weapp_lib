// pages/index/index.js
const app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    list: [{
      text: '按钮',
      name: 'button',
      page: 'button'
    },
    {
      text: '选择框',
      name: 'checkList',
      page: 'checkList'
    }, {
      text: '商品列表',
      name: 'shopList',
      page: 'shopList'
    }, {
      text: '输入框',
      name: 'input',
      page: 'input'
    }, {
      text: '侧滑栏',
      name: 'aside',
      page: 'aside'
    }, {
      text: '列表',
      name: 'list',
      page: 'list'
    }, {
      text: '计数器',
      name: 'stepper',
      page: 'stepper'
    }, {
      text: '底部导航',
      name: 'tabBar',
      page: 'tabBar'
    }, {
      text: '标签页',
      name: 'tabs',
      page: 'tabs'
    }, {
      text: '徽章',
      name: 'badge',
      page: 'badge'
    }, {
      text: '页脚',
      name: 'footer',
      page: 'footer'
    }, {
      text: '加载更多',
      name: 'loadMore',
      page: 'loadMore'
    }, {
      text: '格框',
      name: 'grid',
      page: 'grid'
    }, {
      text: '公告栏',
      name: 'noticebar',
      page: 'noticebar'
    }, {
      text: '徽章',
      name: 'tag',
      page: 'tag'
    }, {
      text: '搜索框',
      name: 'searchbar',
      page: 'searchbar'
    }, {
      text: '弹窗',
      name: 'dialog',
      page: 'dialog'
    }, {
      text: '选择器',
      name: 'picker',
      page: 'picker'
    }, {
      text: '成功提示',
      name: 'toast',
      page: 'toast'
    }, {
      text: '弹出层',
      name: 'popup',
      page: 'popup'
    }, {
      text: '轮播图',
      name: 'swiper',
      page: 'swiper'
    }, {
      text: '折叠面板',
      name: 'accordion',
      page: 'accordion'
    }, {
      text: '时间轴',
      name: 'timeline',
      page: 'timeline'
    }, {
      text: '富文本',
      name: 'richText',
      page: 'richText'
    }, {
      text: '测试',
      name: 'test',
      page: 'test'
    }
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let a = {
      a: 'a'
    }
    let b = {
      a: 'b'
    }
    // console.log({ ...a, ...b });

    let _bind = function (content) {
      let args = Array.prototype.slice.call(arguments, 1);
      let self = this;
      return function () {
        let innerArgs = Array.prototype.slice.call(arguments);
        let finalArgs = innerArgs.concat(args);
        return self.apply(self, finalArgs)
      }
    }

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})