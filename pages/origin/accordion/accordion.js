// pages/accordion/accordion.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    content:'深百果园实业发展有限公司成立于2001年百果园创 始人余惠勇在90年代开展水果贸易过程中发现水果行业 整个供应链上价值损耗浪费巨大，特别是中国是世界水果 产量大国但水果附加价值很低。在全球范围内，还没有企 业整合资源发展水果专营连锁业态感觉有发展空间和价 值挖掘潜力。'
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})