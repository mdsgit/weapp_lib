// pages/tag/tag.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    cartItem:{
      shopId:'1',
      img:'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTI14FjHPCMdWyJkbw5VYx2QwKgiaLyxzGoResJM59RgTmhFV71jNeqFubcj6ic2iaJ0GVWc4A2dPaGhQ/132',
      name:'商品名称商品名称商品名称商品名称商品名称商品名称商品名称商品名称商品名称商品名称商品名称商品名称',
      script:'商品描述商品描述商品描述商品描述商品描述商品描述商品描述商品描述商品描述商品描述商品描述商品描述商品描述商品描述商品描述商品描述',
      nowPrice:'10',
      oldPrice:'100',
      saleNum:'10',
      cartNum:'5',
      actives:[
        /* {
          type:1
        },{
          type:2,
          groupPerson:4
        } */
        ,{
          type:3,
          groupPerson:4
        },
        /* {
          type:4,
          groupPerson:4
        } */
      ]
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})