// pages/dialog/dialog.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  //操作类弹窗
  operating() {
    console.log('操作类弹窗')
    wx.showModal({
      title: '',
      content: '弹窗内容，告知当前状态、信息和解决办法，描述文字尽量控制在三行内',
    })
  },

  //通知类弹窗
  notice() {
    console.log('通知类弹窗')
    wx.showModal({
      title: '',
      content: '弹窗内容，告知当前状态、信息和解决办法，描述文字尽量控制在三行内',
      showCancel: false
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})