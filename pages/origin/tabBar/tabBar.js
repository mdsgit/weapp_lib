// pages/tabBar/tabBar.js
Page({

  /**
   * 页面的初始数据
   */
  data: {},

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let tabBar = {
      color: '',
      selectedColor: 'red',
      backgroundColor: '',
      borderStyle: '',
      position: '',
      list: [{
          pagePath: '',
          text: '',
          iconPath: '',
          selectedIconPath: '',
          num: 10,
          badge: true,
          dot: false,
        },
        {
          pagePath: '',
          text: '首页2首页2首页2首页2首页2首页2首页2首页2首页2',
          iconPath: '',
          selectedIconPath: '',
          num: '100',
          badge: true,
          dot: false
        },
        {
          pagePath: '',
          text: '首页3',
          iconPath: '',
          selectedIconPath: '',
          num: 10,
          badge: true,
          dot: true
        },
        {
          pagePath: '',
          text: '首页3',
          iconPath: '',
          selectedIconPath: '',
          num: 9,
          badge: true,
          dot: false
        }
      ]
    }
    this.setData({
      tabBar
    })
  },

  /**
   * @method navTap
   * @description 获取当前点击的list
   */
  navTap(e) {
    console.log(e.detail.list)
  },

  /**
   * @method getFormId
   * @description 获取formId
   */
  getFormId(e) {
    console.log(e.detail.formId)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})